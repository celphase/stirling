package com.celphase.stirling.mixin

import com.celphase.stirling.worldlet.PhysicsUtil
import net.minecraft.entity.Entity
import net.minecraft.util.math.Vec3d
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.Redirect
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable

@Mixin(Entity::class)
@Suppress("unused", "unused_parameter", "cast_never_succeeds")
class EntityMixin {
    @Inject(at = [At("RETURN")], method = ["adjustMovementForCollisions"], cancellable = true)
    fun stirlingAdjustMovementForCollisions(movement: Vec3d, info: CallbackInfoReturnable<Vec3d>) {
        info.returnValue = PhysicsUtil.adjustMovementForCollisions(this as Entity, info.returnValue)
    }

    /**
     * This mixin fixes a bug in that doesn't occur in vanilla, because in vanilla there's no such thing as diagonal
     * collisions.
     * TODO: This entire system needs to be replaced because it's incompatible with angled walls, instead flatten
     *  velocity based on collision normals.
     */
    @Redirect(method = ["move"], at = At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;setVelocity(DDD)V"))
    fun stirlingRedirectSetVelocity(entity: Entity, x: Double, y: Double, z: Double) {
        val velocity = entity.velocity
        if (x == 0.0) {
            entity.setVelocity(0.0, velocity.y, velocity.z)
        } else {
            entity.setVelocity(velocity.x, velocity.y, 0.0)
        }
    }
}