package com.celphase.stirling.mixin

import net.minecraft.server.network.ServerPlayNetworkHandler
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.gen.Accessor

@Mixin(ServerPlayNetworkHandler::class)
interface ServerPlayNetworkHandlerAccessor {
    @Accessor("floatingTicks")
    fun setFloatingTicks(value: Int)
}