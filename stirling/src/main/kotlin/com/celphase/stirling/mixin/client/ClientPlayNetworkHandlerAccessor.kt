package com.celphase.stirling.mixin.client

import net.minecraft.client.MinecraftClient
import net.minecraft.client.network.ClientPlayNetworkHandler
import net.minecraft.client.world.ClientWorld
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.gen.Accessor

@Mixin(ClientPlayNetworkHandler::class)
interface ClientPlayNetworkHandlerAccessor {
    @Accessor("world")
    fun getWorld() : ClientWorld

    @Accessor("client")
    fun getClient() : MinecraftClient
}
