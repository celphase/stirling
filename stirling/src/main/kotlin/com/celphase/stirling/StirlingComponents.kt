package com.celphase.stirling

import com.celphase.stirling.mechanism.IMechanismRegistryComponent
import com.celphase.stirling.mechanism.MechanismRegistryComponent
import dev.onyxstudios.cca.api.v3.component.ComponentKey
import dev.onyxstudios.cca.api.v3.component.ComponentRegistryV3
import dev.onyxstudios.cca.api.v3.world.WorldComponentFactoryRegistry
import dev.onyxstudios.cca.api.v3.world.WorldComponentInitializer
import net.minecraft.util.Identifier

@Suppress("unused")
class StirlingComponents : WorldComponentInitializer {
    override fun registerWorldComponentFactories(registry: WorldComponentFactoryRegistry) {
        MECHANISM_REGISTRY = ComponentRegistryV3.INSTANCE.getOrCreate(
            Identifier(StirlingMod.MOD_ID, "mechanism_registry"),
            IMechanismRegistryComponent::class.java
        )

        // Add the component to every World instance
        registry.register(MECHANISM_REGISTRY, ::MechanismRegistryComponent)
    }

    companion object {
        lateinit var MECHANISM_REGISTRY: ComponentKey<IMechanismRegistryComponent>
    }
}