package com.celphase.stirling.client.render

import com.celphase.stirling.block.entity.ItemOnAnvilBlockEntity
import net.minecraft.block.AnvilBlock
import net.minecraft.block.Blocks
import net.minecraft.client.MinecraftClient
import net.minecraft.client.render.OverlayTexture
import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.render.WorldRenderer
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher
import net.minecraft.client.render.block.entity.BlockEntityRenderer
import net.minecraft.client.render.model.json.ModelTransformation
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemStack
import net.minecraft.util.math.Quaternion

class ItemOnAnvilBlockEntityRenderer(
    dispatcher: BlockEntityRenderDispatcher
) : BlockEntityRenderer<ItemOnAnvilBlockEntity>(dispatcher) {
    override fun render(
        entity: ItemOnAnvilBlockEntity,
        tickDelta: Float,
        matrices: MatrixStack,
        vertexConsumers: VertexConsumerProvider,
        light: Int,
        overlay: Int
    ) {
        val world = entity.world!!

        val anvilState = world.getBlockState(entity.pos.down())
        val anvilRotation = if (anvilState.block == Blocks.ANVIL) {
            RenderUtil.getFacingQuaternion(anvilState.get(AnvilBlock.FACING), -90.0f)
        } else {
            Quaternion.IDENTITY
        }

        val singleStack = ItemStack(entity.inventory.item, 1)
        val layFlat = entity.inventory.item !is BlockItem
        val startOffset = if (layFlat) 0.015 else 0.125

        for (i in 0 until entity.inventory.count) {
            matrices.push()
            matrices.translate(0.5, startOffset + (0.025 * i), 0.5)
            matrices.scale(0.5f, 0.5f, 0.5f)
            matrices.multiply(anvilRotation)
            matrices.multiply(Quaternion(Vector3f.POSITIVE_Y, -5.0f + (10.0f * i), true))

            if (layFlat) {
                matrices.multiply(Quaternion(Vector3f.POSITIVE_X, 90.0f, true))
            }

            MinecraftClient.getInstance().itemRenderer.renderItem(
                singleStack,
                ModelTransformation.Mode.FIXED,
                light,
                OverlayTexture.DEFAULT_UV,
                matrices,
                vertexConsumers
            )

            matrices.pop()
        }
    }
}