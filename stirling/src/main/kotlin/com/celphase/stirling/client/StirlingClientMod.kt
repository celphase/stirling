package com.celphase.stirling.client

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.client.render.*
import com.celphase.stirling.worldlet.client.WorldletEntityRenderer
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry
import net.minecraft.client.util.ModelIdentifier
import net.minecraft.util.Identifier
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry


class StirlingClientMod : ClientModInitializer {
    override fun onInitializeClient() {
        ModelLoadingRegistry.INSTANCE.registerAppender { _, consumer ->
            consumer.accept(GRINDER_WHEEL_MODEL)
            consumer.accept(AXLE_MODEL)
            consumer.accept(GEAR_MODEL)
            consumer.accept(HAND_CRANK_MODEL)
        }

        val rendererRegistry = BlockEntityRendererRegistry.INSTANCE
        rendererRegistry.register(StirlingMod.GRINDER_BLOCK_ENTITY, ::GrinderBlockEntityRenderer)
        rendererRegistry.register(StirlingMod.AXLE_BLOCK_ENTITY, ::AxleBlockEntityRenderer)
        rendererRegistry.register(StirlingMod.HAND_CRANK_BLOCK_ENTITY, ::HandCrankBlockEntityRenderer)
        rendererRegistry.register(StirlingMod.ITEM_ON_ANVIL_BLOCK_ENTITY, ::ItemOnAnvilBlockEntityRenderer)

        EntityRendererRegistry.INSTANCE.register(StirlingMod.WORLDLET_ENTITY_TYPE, ::WorldletEntityRenderer)
    }

    companion object {
        val GRINDER_WHEEL_MODEL = ModelIdentifier(Identifier(StirlingMod.MOD_ID, "model_grinder_wheel"), "")
        val AXLE_MODEL = ModelIdentifier(Identifier(StirlingMod.MOD_ID, "model_axle"), "")
        val GEAR_MODEL = ModelIdentifier(Identifier(StirlingMod.MOD_ID, "model_gear"), "")
        val HAND_CRANK_MODEL = ModelIdentifier(Identifier(StirlingMod.MOD_ID, "model_hand_crank"), "")
    }
}