package com.celphase.stirling.client.render

import com.celphase.stirling.block.GrinderBlock
import com.celphase.stirling.block.entity.GrinderBlockEntity
import com.celphase.stirling.client.StirlingClientMod
import net.minecraft.client.MinecraftClient
import net.minecraft.client.render.OverlayTexture
import net.minecraft.client.render.RenderLayer
import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher
import net.minecraft.client.render.block.entity.BlockEntityRenderer
import net.minecraft.client.render.model.json.ModelTransformation
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f
import net.minecraft.client.world.ClientWorld
import net.minecraft.util.Identifier
import net.minecraft.util.math.Quaternion

class GrinderBlockEntityRenderer(
    dispatcher: BlockEntityRenderDispatcher
) : BlockEntityRenderer<GrinderBlockEntity>(dispatcher) {
    override fun render(
        entity: GrinderBlockEntity,
        tickDelta: Float,
        matrices: MatrixStack,
        vertexConsumers: VertexConsumerProvider,
        light: Int,
        overlay: Int
    ) {
        val rotation = entity.localRotation.toFloat() + (entity.localVelocity.toFloat() * tickDelta)

        val world = entity.world!! as ClientWorld
        val blockState = world.getBlockState(entity.pos)

        val renderLayer = RenderLayer.getEntityCutoutNoCull(Identifier("minecraft", "textures/atlas/blocks.png"))
        val vertexConsumer = vertexConsumers.getBuffer(renderLayer)

        val model = MinecraftClient.getInstance().bakedModelManager.getModel(StirlingClientMod.GRINDER_WHEEL_MODEL)

        matrices.push()
        matrices.translate(0.5, 0.5, 0.5)
        matrices.multiply(RenderUtil.getFacingQuaternion(blockState.get(GrinderBlock.FACING)))

        // Render the grinding wheel
        matrices.push()
        matrices.multiply(Quaternion(Vector3f.POSITIVE_X, rotation, true))
        matrices.translate(-0.5, -0.5, -0.5)
        RenderUtil.renderBakedModel(
            model,
            matrices.peek(),
            vertexConsumer,
            Vector3f(1.0f, 1.0f, 1.0f),
            light,
            overlay,
        )
        matrices.pop()

        // Render the item inside
        matrices.translate(0.0, -0.2 * entity.getProgressPercentage(), 0.1)
        matrices.scale(2.0f, 2.0f, 2.0f)
        MinecraftClient.getInstance().itemRenderer.renderItem(
            entity.inventory,
            ModelTransformation.Mode.GROUND,
            light,
            OverlayTexture.DEFAULT_UV,
            matrices,
            vertexConsumers
        )

        matrices.pop()
    }
}