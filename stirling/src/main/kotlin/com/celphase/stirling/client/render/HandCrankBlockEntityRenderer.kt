package com.celphase.stirling.client.render

import com.celphase.stirling.block.HandCrankBlock
import com.celphase.stirling.block.entity.HandCrankBlockEntity
import com.celphase.stirling.client.StirlingClientMod
import net.minecraft.client.MinecraftClient
import net.minecraft.client.render.RenderLayer
import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher
import net.minecraft.client.render.block.entity.BlockEntityRenderer
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f
import net.minecraft.client.world.ClientWorld
import net.minecraft.util.Identifier
import net.minecraft.util.math.Quaternion

class HandCrankBlockEntityRenderer(dispatcher: BlockEntityRenderDispatcher) :
    BlockEntityRenderer<HandCrankBlockEntity>(dispatcher) {
    override fun render(
        entity: HandCrankBlockEntity,
        tickDelta: Float,
        matrices: MatrixStack,
        vertexConsumers: VertexConsumerProvider,
        light: Int,
        overlay: Int
    ) {
        val rotation = entity.localRotation.toFloat() + (tickDelta * entity.localVelocity.toFloat())

        val world = entity.world!! as ClientWorld
        val blockState = world.getBlockState(entity.pos)

        val renderLayer = RenderLayer.getEntityCutoutNoCull(Identifier("minecraft", "textures/atlas/blocks.png"))
        val vertexConsumer = vertexConsumers.getBuffer(renderLayer)

        val model = MinecraftClient.getInstance().bakedModelManager.getModel(StirlingClientMod.HAND_CRANK_MODEL)

        matrices.push()
        matrices.translate(0.5, 0.5, 0.5)
        matrices.multiply(RenderUtil.getFacingQuaternion(blockState.get(HandCrankBlock.FACING)))
        matrices.multiply(Quaternion(Vector3f.POSITIVE_Z, rotation, true))
        matrices.translate(-0.5, -0.5, -0.5)

        RenderUtil.renderBakedModel(
            model,
            matrices.peek(),
            vertexConsumer,
            Vector3f(1.0f, 1.0f, 1.0f),
            light,
            overlay,
        )

        matrices.pop()
    }
}