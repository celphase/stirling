package com.celphase.stirling.client.render

import net.minecraft.client.render.VertexConsumer
import net.minecraft.client.render.model.BakedModel
import net.minecraft.client.render.model.BakedQuad
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f
import net.minecraft.client.util.math.Vector4f
import net.minecraft.util.math.Direction
import net.minecraft.util.math.Quaternion
import java.util.*

object RenderUtil {
    fun renderBakedModel(
        model: BakedModel,
        matrixEntry: MatrixStack.Entry,
        vertexConsumer: VertexConsumer,
        color: Vector3f,
        light: Int,
        overlay: Int,
    ) {
        val random = Random()

        for (direction in Direction.values()) {
            random.setSeed(42)
            val list = model.getQuads(null, direction, random)
            if (list.isNotEmpty()) {
                this.renderQuads(
                    matrixEntry,
                    vertexConsumer,
                    list,
                    color,
                    light,
                    overlay
                )
            }
        }

        random.setSeed(42)
        val list2 = model.getQuads(null, null as Direction?, random)
        if (list2.isNotEmpty()) {
            renderQuads(
                matrixEntry,
                vertexConsumer,
                list2,
                color,
                light,
                overlay,
            )
        }
    }

    private fun renderQuads(
        matrixEntry: MatrixStack.Entry,
        vertexConsumer: VertexConsumer,
        quads: List<BakedQuad>,
        color: Vector3f,
        light: Int,
        overlay: Int,
    ) {
        for (bakedQuad in quads) {
            renderQuad(
                vertexConsumer,
                matrixEntry,
                bakedQuad,
                color,
                light,
                overlay,
            )
        }
    }

    private fun renderQuad(
        vertexConsumer: VertexConsumer,
        matrixEntry: MatrixStack.Entry,
        quad: BakedQuad,
        color: Vector3f,
        light: Int,
        overlay: Int,
    ) {
        val vertexData = quad.vertexData
        // Determine at runtime for optifine compatibility
        val stride = vertexData.size / 4

        val normal = getQuadNormal(vertexData, stride)
        normal.transform(matrixEntry.normal)

        // Only apply color if this quad needs it
        var usedColor = color
        if (!quad.hasColor()) {
            usedColor = Vector3f(1.0f, 1.0f, 1.0f)
        }

        for (vertex_i in 0 until 4) {
            val offset = vertex_i * stride

            val x = vertexData.getFloat(offset)
            val y = vertexData.getFloat(offset + 1)
            val z = vertexData.getFloat(offset + 2)
            val position = Vector4f(x, y, z, 1.0f)
            position.transform(matrixEntry.model)

            val u = vertexData.getFloat(offset + 4)
            val v = vertexData.getFloat(offset + 5)

            vertexConsumer.vertex(
                position.x,
                position.y,
                position.z,
                usedColor.x,
                usedColor.y,
                usedColor.z,
                1.0f,
                u,
                v,
                overlay,
                light,
                normal.x,
                normal.y,
                normal.z,
            )
        }
    }

    /**
     * Derive the normal from vertex data positions.
     */
    private fun getQuadNormal(vertexData: IntArray, stride: Int): Vector3f {
        val position0 = Vector3f(
            vertexData.getFloat(0),
            vertexData.getFloat(1),
            vertexData.getFloat(2),
        )
        val position1 = Vector3f(
            vertexData.getFloat(stride),
            vertexData.getFloat(stride + 1),
            vertexData.getFloat(stride + 2),
        )
        val offset = stride * 2
        val position2 = Vector3f(
            vertexData.getFloat(offset),
            vertexData.getFloat(offset + 1),
            vertexData.getFloat(offset + 2),
        )

        position1.subtract(position0)
        position2.subtract(position0)

        position1.cross(position2)
        position1.normalize()

        return position1
    }

    fun getFacingQuaternion(direction: Direction, yOffset: Float = 0.0f): Quaternion =
        when (direction) {
            Direction.NORTH -> Quaternion(Vector3f.POSITIVE_Y, 0f + yOffset, true)
            Direction.SOUTH -> Quaternion(Vector3f.POSITIVE_Y, 180f + yOffset, true)
            Direction.EAST -> Quaternion(Vector3f.POSITIVE_Y, 270f + yOffset, true)
            Direction.WEST -> Quaternion(Vector3f.POSITIVE_Y, 90f + yOffset, true)
            Direction.UP -> Quaternion(Vector3f.POSITIVE_X, 90.0f, true)
            Direction.DOWN -> Quaternion(Vector3f.POSITIVE_X, -90.0f, true)
        }

    fun getAxisQuaternion(axis: Direction.Axis): Quaternion =
        when (axis) {
            Direction.Axis.X -> Quaternion(Vector3f.POSITIVE_Y, 90f, true)
            Direction.Axis.Y -> Quaternion(Vector3f.POSITIVE_X, -90f, true)
            Direction.Axis.Z -> Quaternion(Vector3f.POSITIVE_Y, 0f, true)
        }
}

fun IntArray.getFloat(index: Int): Float =
    java.lang.Float.intBitsToFloat(this[index])
