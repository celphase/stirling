package com.celphase.stirling

import com.celphase.stirling.block.*
import com.celphase.stirling.block.entity.*
import com.celphase.stirling.worldlet.entity.WorldletEntity
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.fabricmc.fabric.api.`object`.builder.v1.entity.FabricEntityTypeBuilder
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientBlockEntityEvents
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerBlockEntityEvents
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.block.Block
import net.minecraft.block.Material
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.entity.EntityType
import net.minecraft.entity.SpawnGroup
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.recipe.Recipe
import net.minecraft.recipe.RecipeType
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class StirlingMod : ModInitializer {
    override fun onInitialize() {
        LOGGER.info("Initializing Stirling")

        MACHINE_CASING_BLOCK = Block(FabricBlockSettings.of(Material.METAL).strength(2.5f))
        GRINDER_BLOCK = GrinderBlock(FabricBlockSettings.of(Material.METAL).strength(2.5f).nonOpaque())
        AXLE_BLOCK = AxleBlock(FabricBlockSettings.of(Material.METAL).strength(1.5f).nonOpaque())
        HAND_CRANK_BLOCK = HandCrankBlock(FabricBlockSettings.of(Material.METAL).strength(1.5f).nonOpaque())
        ITEM_ON_ANVIL_BLOCK = ItemOnAnvilBlock(FabricBlockSettings.of(Material.METAL).strength(0.5f).nonOpaque())
        DEBUG_SPINNER_BLOCK = DebugSpinnerBlock(FabricBlockSettings.of(Material.METAL).strength(2.5f))

        MACHINE_CASING_BLOCK_ITEM = BlockItem(MACHINE_CASING_BLOCK, Item.Settings().group(ItemGroup.DECORATIONS))
        GRINDER_WHEEL_ITEM = Item(FabricItemSettings().group(ItemGroup.MATERIALS))
        GRINDER_BLOCK_ITEM = BlockItem(GRINDER_BLOCK, Item.Settings().group(ItemGroup.DECORATIONS))
        SMITHS_HAMMER_ITEM = Item(FabricItemSettings().group(ItemGroup.TOOLS).maxCount(1))
        IRON_ROD_ITEM = Item(FabricItemSettings().group(ItemGroup.MATERIALS))
        IRON_PLATE_ITEM = Item(FabricItemSettings().group(ItemGroup.MATERIALS))
        AXLE_ITEM = BlockItem(AXLE_BLOCK, Item.Settings().group(ItemGroup.MISC))
        GEAR_ITEM = Item(Item.Settings().group(ItemGroup.MISC))
        HAND_CRANK_ITEM = BlockItem(HAND_CRANK_BLOCK, Item.Settings().group(ItemGroup.DECORATIONS))
        IRON_ORE_CHUNKS = Item(FabricItemSettings().group(ItemGroup.MATERIALS))
        DEBUG_SPINNER_BLOCK_ITEM = BlockItem(DEBUG_SPINNER_BLOCK, Item.Settings().group(ItemGroup.DECORATIONS))

        GRINDER_BLOCK_ENTITY = BlockEntityType.Builder.create(::GrinderBlockEntity, GRINDER_BLOCK).build(null)
        AXLE_BLOCK_ENTITY = BlockEntityType.Builder.create(::AxleBlockEntity, AXLE_BLOCK).build(null)
        HAND_CRANK_BLOCK_ENTITY = BlockEntityType.Builder.create(::HandCrankBlockEntity, HAND_CRANK_BLOCK).build(null)
        ITEM_ON_ANVIL_BLOCK_ENTITY =
            BlockEntityType.Builder.create(::ItemOnAnvilBlockEntity, ITEM_ON_ANVIL_BLOCK).build(null)
        DEBUG_SPINNER_BLOCK_ENTITY =
            BlockEntityType.Builder.create(::DebugSpinnerBlockEntity, DEBUG_SPINNER_BLOCK).build(null)

        GRINDING_RECIPE_TYPE = createRecipeType("grinding")
        GRINDING_RECIPE_SERIALIZER = GrindingRecipeSerializer()

        WORLDLET_ENTITY_TYPE =
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, ::WorldletEntity).disableSaving().build()

        Registry.register(Registry.BLOCK, Identifier(MOD_ID, "machine_casing"), MACHINE_CASING_BLOCK)
        Registry.register(Registry.BLOCK, Identifier(MOD_ID, "grinder"), GRINDER_BLOCK)
        Registry.register(Registry.BLOCK, Identifier(MOD_ID, "axle"), AXLE_BLOCK)
        Registry.register(Registry.BLOCK, Identifier(MOD_ID, "hand_crank"), HAND_CRANK_BLOCK)
        Registry.register(Registry.BLOCK, Identifier(MOD_ID, "item_on_anvil"), ITEM_ON_ANVIL_BLOCK)
        Registry.register(Registry.BLOCK, Identifier(MOD_ID, "debug_spinner"), DEBUG_SPINNER_BLOCK)

        Registry.register(Registry.ITEM, Identifier(MOD_ID, "machine_casing"), MACHINE_CASING_BLOCK_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "grinder_wheel"), GRINDER_WHEEL_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "grinder"), GRINDER_BLOCK_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "smiths_hammer"), SMITHS_HAMMER_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "iron_rod"), IRON_ROD_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "iron_plate"), IRON_PLATE_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "axle"), AXLE_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "gear"), GEAR_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "hand_crank"), HAND_CRANK_ITEM)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "iron_ore_chunks"), IRON_ORE_CHUNKS)
        Registry.register(Registry.ITEM, Identifier(MOD_ID, "debug_spinner"), DEBUG_SPINNER_BLOCK_ITEM)

        Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(MOD_ID, "grinder"), GRINDER_BLOCK_ENTITY)
        Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(MOD_ID, "axle"), AXLE_BLOCK_ENTITY)
        Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(MOD_ID, "hand_crank"), HAND_CRANK_BLOCK_ENTITY)
        Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(MOD_ID, "item_on_anvil"), ITEM_ON_ANVIL_BLOCK_ENTITY)
        Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(MOD_ID, "debug_spinner"), DEBUG_SPINNER_BLOCK_ENTITY)

        Registry.register(Registry.RECIPE_TYPE, Identifier(MOD_ID, "grinding"), GRINDING_RECIPE_TYPE)
        Registry.register(Registry.RECIPE_SERIALIZER, Identifier(MOD_ID, "grinding"), GRINDING_RECIPE_SERIALIZER)

        Registry.register(Registry.ENTITY_TYPE, Identifier(MOD_ID, "worldlet"), WORLDLET_ENTITY_TYPE)

        ServerBlockEntityEvents.BLOCK_ENTITY_LOAD.register { e, _ -> onServerBlockEntityLoad(e) }
        ServerBlockEntityEvents.BLOCK_ENTITY_UNLOAD.register { e, _ -> onServerBlockEntityUnload(e) }

        ClientBlockEntityEvents.BLOCK_ENTITY_LOAD.register { e, _ -> onClientBlockEntityLoad(e) }
        ClientBlockEntityEvents.BLOCK_ENTITY_UNLOAD.register { e, _ -> onClientBlockEntityUnload(e) }
    }

    private fun onServerBlockEntityLoad(e: BlockEntity) {
        if (e is IServerBlockEntityLifecycle) {
            e.onServerLoad()
        }
    }

    private fun onServerBlockEntityUnload(e: BlockEntity) {
        if (e is IServerBlockEntityLifecycle) {
            e.onServerUnload()
        }
    }

    private fun onClientBlockEntityLoad(e: BlockEntity) {
        if (e is IClientBlockEntityLifecycle) {
            e.onClientLoad()
        }
    }

    private fun onClientBlockEntityUnload(e: BlockEntity) {
        if (e is IClientBlockEntityLifecycle) {
            e.onClientUnload()
        }
    }

    companion object {
        const val MOD_ID = "stirling"
        val LOGGER: Logger = LogManager.getLogger(MOD_ID)

        lateinit var MACHINE_CASING_BLOCK: Block
        lateinit var GRINDER_BLOCK: GrinderBlock
        lateinit var AXLE_BLOCK: AxleBlock
        lateinit var HAND_CRANK_BLOCK: HandCrankBlock
        lateinit var ITEM_ON_ANVIL_BLOCK: ItemOnAnvilBlock
        lateinit var DEBUG_SPINNER_BLOCK: DebugSpinnerBlock

        lateinit var MACHINE_CASING_BLOCK_ITEM: BlockItem
        lateinit var GRINDER_WHEEL_ITEM: Item
        lateinit var GRINDER_BLOCK_ITEM: BlockItem
        lateinit var SMITHS_HAMMER_ITEM: Item
        lateinit var IRON_ROD_ITEM: Item
        lateinit var IRON_PLATE_ITEM: Item
        lateinit var AXLE_ITEM: Item
        lateinit var GEAR_ITEM: Item
        lateinit var HAND_CRANK_ITEM: Item
        lateinit var IRON_ORE_CHUNKS: Item
        lateinit var DEBUG_SPINNER_BLOCK_ITEM: BlockItem

        lateinit var GRINDER_BLOCK_ENTITY: BlockEntityType<GrinderBlockEntity>
        lateinit var AXLE_BLOCK_ENTITY: BlockEntityType<AxleBlockEntity>
        lateinit var HAND_CRANK_BLOCK_ENTITY: BlockEntityType<HandCrankBlockEntity>
        lateinit var ITEM_ON_ANVIL_BLOCK_ENTITY: BlockEntityType<ItemOnAnvilBlockEntity>
        lateinit var DEBUG_SPINNER_BLOCK_ENTITY: BlockEntityType<DebugSpinnerBlockEntity>

        lateinit var GRINDING_RECIPE_TYPE: RecipeType<GrindingRecipe>
        lateinit var GRINDING_RECIPE_SERIALIZER: GrindingRecipeSerializer

        lateinit var WORLDLET_ENTITY_TYPE: EntityType<WorldletEntity>

        private fun <T : Recipe<*>> createRecipeType(name: String): RecipeType<T> {
            return object : RecipeType<T> {
                override fun toString(): String {
                    return name
                }
            }
        }
    }
}