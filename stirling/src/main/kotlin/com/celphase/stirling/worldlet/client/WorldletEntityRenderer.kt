package com.celphase.stirling.worldlet.client

import com.celphase.stirling.client.render.RenderUtil
import com.celphase.stirling.worldlet.entity.WorldletEntity
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry
import net.minecraft.block.Blocks
import net.minecraft.client.MinecraftClient
import net.minecraft.client.render.RenderLayers
import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.render.entity.EntityRenderDispatcher
import net.minecraft.client.render.entity.EntityRenderer
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f
import net.minecraft.util.Identifier
import net.minecraft.util.math.Quaternion

@Suppress("unused_parameter")
class WorldletEntityRenderer(dispatcher: EntityRenderDispatcher, context: EntityRendererRegistry.Context) :
    EntityRenderer<WorldletEntity>(dispatcher) {
    override fun getTexture(entity: WorldletEntity): Identifier? = null

    override fun render(
        entity: WorldletEntity,
        yaw: Float,
        tickDelta: Float,
        matrices: MatrixStack,
        vertexConsumers: VertexConsumerProvider,
        light: Int
    ) {
        val state = Blocks.GRASS_BLOCK.defaultState
        val renderLayer = RenderLayers.getEntityBlockLayer(state, false)
        val vertexConsumer = vertexConsumers.getBuffer(renderLayer)

        val client = MinecraftClient.getInstance()
        val axleModel = client.blockRenderManager.getModel(state)

        val i: Int = client.blockColors.getColor(state, null, null, 0)
        val color = Vector3f(
            (i shr 16 and 255).toFloat() / 255.0f,
            (i shr 8 and 255).toFloat() / 255.0f,
            (i and 255).toFloat() / 255.0f,
        )

        matrices.push()
        matrices.multiply(Quaternion(Vector3f.POSITIVE_X, entity.yaw, true))

        RenderUtil.renderBakedModel(
            axleModel,
            matrices.peek(),
            vertexConsumer,
            color,
            light,
            655360,
        )

        matrices.pop()
    }
}
