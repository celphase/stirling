package com.celphase.stirling.worldlet.entity

import net.minecraft.entity.Entity
import net.minecraft.entity.EntityType
import net.minecraft.nbt.CompoundTag
import net.minecraft.network.Packet
import net.minecraft.util.math.Box
import net.minecraft.util.math.Vec3d
import net.minecraft.world.World

class WorldletEntity(
    entityType: EntityType<out WorldletEntity>,
    world: World,
) : Entity(entityType, world) {
    override fun setBoundingBox(boundingBox: Box) {
        // Temporary placeholder, this needs to correctly calculate bounds for rotation
        val globalBox = Box(pos.subtract(1.0, 1.0, 1.0), pos.add(Vec3d(2.0, 2.0, 2.0)))
        super.setBoundingBox(globalBox)
    }

    override fun initDataTracker() {
    }

    override fun readCustomDataFromTag(tag: CompoundTag) {
        yaw = 45.0f
    }

    override fun writeCustomDataToTag(tag: CompoundTag) {
    }

    override fun createSpawnPacket(): Packet<*> {
        return WorldletSpawnS2CPacket(this)
    }
}
