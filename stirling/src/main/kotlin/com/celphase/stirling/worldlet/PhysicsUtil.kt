package com.celphase.stirling.worldlet

import com.celphase.stirling.StirlingMod
import net.minecraft.client.util.math.Vector3f
import net.minecraft.entity.Entity
import net.minecraft.util.math.Box
import net.minecraft.util.math.Quaternion
import net.minecraft.util.math.Vec3d
import kotlin.math.abs
import kotlin.math.sign

object PhysicsUtil {
    fun adjustMovementForCollisions(entity: Entity, inMovement: Vec3d): Vec3d {
        val boundingBox = entity.boundingBox
        val extents = extentsFromBB(boundingBox)

        val broadphaseBox = boundingBox.union(boundingBox.offset(inMovement))
        val worldlets = entity.world.getEntitiesByType(StirlingMod.WORLDLET_ENTITY_TYPE, broadphaseBox) { true }

        var movement = inMovement

        for (worldlet in worldlets) {
            val localBounds = Box(Vec3d(0.0, 0.0, 0.0), Vec3d(1.0, 1.0, 1.0))

            val worldletQuaternion = Quaternion(Vector3f.POSITIVE_X, -worldlet.yaw, true)
            val worldletMatrix = Matrix3d(worldletQuaternion)

            val translatedCenter = boundingBox.center.subtract(worldlet.pos)
            val localCenter = worldletMatrix.transform(translatedCenter)

            val separation = separateBBs(
                localBounds.center,
                localCenter,
                extentsFromBB(localBounds),
                extents,
                worldletMatrix,
                movement,
            )

            separation?.let {
                worldletMatrix.transpose()
                movement = movement.add(worldletMatrix.transform(it))
            }
        }

        return movement
    }

    private fun extentsFromBB(bb: Box): Vec3d {
        return Vec3d(bb.xLength / 2, bb.yLength / 2, bb.zLength / 2)
    }

    private fun separateBBs(cA: Vec3d, cB: Vec3d, eA: Vec3d, eB: Vec3d, m: Matrix3d, motion: Vec3d): Vec3d? {
        val mf = SeparationManifold()
        val t = cB.subtract(cA)

        val localMotion = m.transform(motion)

        val a00 = abs(m.m00)
        val a01 = abs(m.m01)
        val a02 = abs(m.m02)
        val a10 = abs(m.m10)
        val a11 = abs(m.m11)
        val a12 = abs(m.m12)
        val a20 = abs(m.m20)
        val a21 = abs(m.m21)
        val a22 = abs(m.m22)

        // Identity axes
        val uA0 = Vec3d(1.0, 0.0, 0.0)
        val uA1 = Vec3d(0.0, 1.0, 0.0)
        val uA2 = Vec3d(0.0, 0.0, 1.0)

        val uB0 = Vec3d(m.m00, m.m10, m.m20)
        val uB1 = Vec3d(m.m01, m.m11, m.m21)
        val uB2 = Vec3d(m.m02, m.m12, m.m22)

        val isSeparated =
            // Separate along A's local axes
            checkSeparationAlong(mf, uA0, t.x, eA.x, a00 * eB.x + a01 * eB.y + a02 * eB.z, localMotion.x) ||
            checkSeparationAlong(mf, uA1, t.y, eA.y, a10 * eB.x + a11 * eB.y + a12 * eB.z, localMotion.y) ||
            checkSeparationAlong(mf, uA2, t.z, eA.z, a20 * eB.x + a21 * eB.y + a22 * eB.z, localMotion.z) ||

            // Separate along B's local axes (global XYZ)
            checkSeparationAlong(mf, uB0, t.x * m.m00 + t.y * m.m10 + t.z * m.m20, eA.x * a00 + eA.y * a10 + eA.z * a20, eB.x, motion.x) ||
            checkSeparationAlong(mf, uB1, t.x * m.m01 + t.y * m.m11 + t.z * m.m21, eA.x * a01 + eA.y * a11 + eA.z * a21, eB.y, motion.y) ||
            checkSeparationAlong(mf, uB2, t.x * m.m02 + t.y * m.m12 + t.z * m.m22, eA.x * a02 + eA.y * a12 + eA.z * a22, eB.z, motion.z)

        if (!isSeparated) {
            return mf.toVector()
        }

        return null
    }

    private fun checkSeparationAlong(
        mf: SeparationManifold,
        axis: Vec3d,
        distance: Double,
        extentA: Double,
        extentB: Double,
        motion: Double,
    ): Boolean {
        // Check if at our projected distance we have a collision
        val projectedDistance = distance + motion
        val intersection = (extentA + extentB) - abs(projectedDistance)
        if (intersection <= 0.0) return true

        // Prefer axes where the intersection happened during the motion
        val separation = intersection * sign(projectedDistance)
        val preferAxis = abs(motion) >= intersection && (separation < 0.0) == (motion > 0.0)

        if (preferAxis || !mf.hasPreferential) {
            val becamePreferred = !mf.hasPreferential && preferAxis

            if (becamePreferred || intersection < abs(mf.separation)) {
                mf.hasPreferential = preferAxis
                mf.axis = axis.normalize()
                mf.separation = separation
            }
        }

        return false
    }

    /* Untested but may be useful some day:
    private fun rotateVector(vector: Vec3d, quaternion: Quaternion): Vec3d {
        // Extract the vector and scalar parts of the quaternion
        val u = Vec3d(quaternion.x.toDouble(), quaternion.y.toDouble(), quaternion.z.toDouble())
        val s = quaternion.w.toDouble()

        // Do the math
        val uv = cross(u, vector)
        val uuv = cross(u, uv)

        return vector.add((uv.multiply(s) + uuv) * 2.0)
    }

    private fun cross(a: Vec3d, b: Vec3d): Vec3d =
        Vec3d(
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x,
        )*/

    class SeparationManifold {
        var hasPreferential = false
        var axis: Vec3d = Vec3d.ZERO
        var separation: Double = Double.MAX_VALUE

        fun toVector(): Vec3d {
            // Add a small amount extra to make sure the collision is resolved
            val sign = sign(separation)
            val epsilon = 1E-4
            return axis.multiply((abs(separation) + epsilon) * sign)
        }
    }
}