package com.celphase.stirling.mechanism

import dev.onyxstudios.cca.api.v3.component.ComponentV3
import dev.onyxstudios.cca.api.v3.component.tick.ClientTickingComponent
import dev.onyxstudios.cca.api.v3.component.tick.ServerTickingComponent
import java.util.*

interface IMechanismRegistryComponent : ComponentV3, ClientTickingComponent, ServerTickingComponent {
    fun createMechanism(): Mechanism

    fun removeMechanism(id: UUID)

    /**
     * Detaches a part from a machine, removing it.
     * - Remove the part from the mechanism it's assigned to
     * - Split up disconnected mechanisms
     */
    fun detachPart(part: MechanismPart)

    fun getMechanism(id: UUID): Mechanism?
}