package com.celphase.stirling.mechanism.entity

import com.celphase.stirling.StirlingComponents
import com.celphase.stirling.StirlingMod
import com.celphase.stirling.mechanism.IMechanismRegistryComponent
import com.celphase.stirling.mechanism.MechanismPart
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.world.World

object BlockMechanism {
    /**
     * Creates a part at a location.
     * - Attempt to connect to existing mechanisms.
     * - Join mechanisms together.
     * - Create a new mechanism if necessary.
     */
    fun createPartAt(world: World, pos: BlockPos, connections: List<Direction>): MechanismPart {
        if (world.isClient) {
            throw IllegalStateException("createPartAt can only be called on the server")
        }

        val registry = StirlingComponents.MECHANISM_REGISTRY.get(world)

        // Scan the neighboring connections for existing parts
        val connectedParts = getConnectedParts(world, pos, connections)

        // If we have no connecting existing parts
        if (connectedParts.isEmpty()) {
            // Create a new machine for this part
            val mechanism = registry.createMechanism()

            return mechanism.createPart()
        }

        // If we do have one, use the mechanism of the first we found
        val part = connectedParts[0].mechanism.createPart()
        connectedParts[0].connections += part
        part.connections += connectedParts[0]

        // For any further connections, merge them with the existing one if they have a different mechanism
        for (i in 1 until connectedParts.size) {
            mergeConnect(registry, part, connectedParts[i])
        }

        return part
    }

    fun reconnectPartAt(part: MechanismPart, world: World, pos: BlockPos, connections: List<Direction>) {
        if (world.isClient) {
            throw IllegalStateException("reconnectPartAt can only be called on the server")
        }

        val connectedParts = getConnectedParts(world, pos, connections)

        // Figure out which connections were added, and which ones were removed
        val newParts = connectedParts.filter { !part.connections.contains(it) }
        val removedParts = part.connections.filter { !connectedParts.contains(it) }

        // Merge-connect the new parts
        val registry = StirlingComponents.MECHANISM_REGISTRY.get(world)
        for (newPart in newParts) {
            mergeConnect(registry, part, newPart)
        }

        // TODO: Remove connections
        if (removedParts.isNotEmpty()) {
            StirlingMod.LOGGER.error("Remove connections not yet implemented")
        }
    }

    private fun mergeConnect(registry: IMechanismRegistryComponent, part: MechanismPart, connectedPart: MechanismPart) {
        connectedPart.connections += part
        part.connections += connectedPart

        // If we're already in the same mechanism, don't do anything else
        if (connectedPart.mechanism == part.mechanism) {
            return
        }

        val oldMechanism = connectedPart.mechanism
        part.mechanism.mergeFrom(oldMechanism)
        registry.removeMechanism(oldMechanism.id)
    }

    private fun getConnectedParts(world: World, pos: BlockPos, connections: List<Direction>) =
        connections.mapNotNull { getPartForConnection(world, pos, it) }

    private fun getPartForConnection(world: World, pos: BlockPos, connection: Direction): MechanismPart? {
        val entity = world.getBlockEntity(pos.offset(connection))

        if (entity !is IPartBlockEntity) {
            return null
        }

        return entity.getPartFor(connection.opposite)
    }
}
