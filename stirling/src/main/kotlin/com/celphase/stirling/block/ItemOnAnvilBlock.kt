package com.celphase.stirling.block

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.block.entity.ItemOnAnvilBlockEntity
import net.minecraft.block.*
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.tag.BlockTags
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.shape.VoxelShape
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.BlockView
import net.minecraft.world.World

class ItemOnAnvilBlock(settings: Settings) : Block(settings), BlockEntityProvider {
    override fun createBlockEntity(world: BlockView?): BlockEntity =
        ItemOnAnvilBlockEntity()

    override fun getRenderType(state: BlockState?): BlockRenderType =
        BlockRenderType.ENTITYBLOCK_ANIMATED

    override fun getOutlineShape(
        state: BlockState?,
        world: BlockView?,
        pos: BlockPos?,
        context: ShapeContext?
    ): VoxelShape {
        return VoxelShapes.cuboid(0.3, 0.0, 0.3, 0.7, 0.05, 0.7)
    }

    override fun neighborUpdate(
        state: BlockState,
        world: World,
        pos: BlockPos,
        block: Block,
        fromPos: BlockPos,
        notify: Boolean
    ) {
        if (fromPos != pos.down()) {
            return
        }

        // Make sure we still have an anvil under us
        val below = world.getBlockState(fromPos)
        if (!BlockTags.ANVIL.contains(below.block)) {
            world.breakBlock(pos, true)
        }
    }

    override fun onUse(
        state: BlockState,
        world: World,
        pos: BlockPos,
        player: PlayerEntity,
        hand: Hand,
        hit: BlockHitResult,
    ): ActionResult {
        // Make sure the player is in fact holding the tool
        if (player.getStackInHand(hand).item != StirlingMod.SMITHS_HAMMER_ITEM) {
            return ActionResult.PASS
        }

        val entity = world.getBlockEntity(pos) as ItemOnAnvilBlockEntity

        return if (entity.performCraft(player)) {
            ActionResult.SUCCESS
        } else {
            ActionResult.FAIL
        }
    }

    override fun afterBreak(
        world: World,
        player: PlayerEntity,
        pos: BlockPos,
        state: BlockState,
        blockEntity: BlockEntity?,
        stack: ItemStack?,
    ) {
        // Drop the contents
        val entity = blockEntity as ItemOnAnvilBlockEntity
        dropStack(world, pos, entity.inventory)

        super.afterBreak(world, player, pos, state, blockEntity, stack)
    }

    companion object {
        fun tryAddToAnvil(world: World, anvilPos: BlockPos, stack: ItemStack) {
            if (!isValidIngredient(stack.item)) {
                return
            }

            if (!tryAddOrPlace(world, anvilPos, stack.item)) {
                return
            }

            // We did something, remove the iron ingot from the inventory
            world.playSound(null, anvilPos, SoundEvents.BLOCK_ANVIL_PLACE, SoundCategory.BLOCKS, 0.2f, 1.0f)
            stack.decrement(1)
        }

        fun isValidIngredient(item: Item): Boolean {
            return item == Items.IRON_INGOT || item == Items.IRON_BLOCK
        }

        private fun tryAddOrPlace(world: World, anvilPos: BlockPos, item: Item): Boolean {
            // Check if we can place an iron ingot block above this
            val above = anvilPos.up()
            val stateAbove = world.getBlockState(above)

            if (stateAbove.block == StirlingMod.ITEM_ON_ANVIL_BLOCK) {
                // It's already there, add to it
                val entity = world.getBlockEntity(above) as ItemOnAnvilBlockEntity
                if (!entity.addToStack(item)) {
                    return false
                }
            } else {
                // It's not already there, try to place it
                val blockState = StirlingMod.ITEM_ON_ANVIL_BLOCK.defaultState

                // Make sure we can place it
                if (!stateAbove.isAir || !world.canPlace(blockState, above, ShapeContext.absent())) {
                    return false
                }

                // Add the block to the world above the anvil
                world.setBlockState(above, blockState)

                // Set the item in the block
                val entity = world.getBlockEntity(above) as ItemOnAnvilBlockEntity
                entity.setItem(item)
            }

            return true
        }
    }
}