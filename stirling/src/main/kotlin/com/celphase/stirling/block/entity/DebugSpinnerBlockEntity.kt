package com.celphase.stirling.block.entity

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.worldlet.entity.WorldletEntity
import net.minecraft.block.entity.BlockEntity
import net.minecraft.util.Tickable
import kotlin.math.sin

class DebugSpinnerBlockEntity : BlockEntity(StirlingMod.DEBUG_SPINNER_BLOCK_ENTITY), Tickable {
    private var worldlet: WorldletEntity? = null
    private var progress = 0.0f

    override fun tick() {
        if (worldlet == null) {
            val worldlet = WorldletEntity(StirlingMod.WORLDLET_ENTITY_TYPE, world!!)
            worldlet.updatePosition(
                pos.x.toDouble() + 0.5,
                pos.y.toDouble() + 0.5,
                pos.z.toDouble() + 1.5,
            )

            world!!.spawnEntity(worldlet)
            this.worldlet = worldlet
        }

        progress += 0.1f
        worldlet!!.updatePosition(
            pos.x.toDouble() + 0.5 + sin(progress),
            pos.y.toDouble() + 0.5,
            pos.z.toDouble() + 1.5,
        )
    }
}
