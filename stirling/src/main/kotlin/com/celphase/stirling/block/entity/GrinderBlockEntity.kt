package com.celphase.stirling.block.entity

import com.celphase.stirling.GrindingRecipe
import com.celphase.stirling.StirlingMod
import com.celphase.stirling.block.GrinderBlock
import com.celphase.stirling.mechanism.entity.SinglePartBlockEntity
import net.minecraft.block.BlockState
import net.minecraft.entity.ItemEntity
import net.minecraft.inventory.Inventories
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.nbt.CompoundTag
import net.minecraft.predicate.entity.EntityPredicates
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.util.collection.DefaultedList
import net.minecraft.util.math.Direction
import net.minecraft.util.shape.VoxelShapes

class GrinderBlockEntity : SinglePartBlockEntity(StirlingMod.GRINDER_BLOCK_ENTITY) {
    var inventory: ItemStack = ItemStack.EMPTY
        private set

    private var activeRecipe: GrindingRecipe? = null

    private var progress: Int = 0

    fun getProgressPercentage(): Float = progress / 1000.0f

    override fun tick() {
        super.tick()

        if (inventory == ItemStack.EMPTY) {
            tryPickupItem()
        } else {
            if (progress < 1000) {
                process()
            } else {
                trySpawnResult()
            }
        }
    }

    private fun process() {
        // Load the recipe if it wasn't loaded yet
        if (activeRecipe == null) {
            activeRecipe = getRecipeForInput(inventory)
        }

        // Must grind the right way
        progress += maxOf(localVelocity, 0)

        if (!world!!.isClient) {
            markDirty()
        }
    }

    private fun tryPickupItem() {
        if (world!!.isClient) {
            return
        }

        // Check for valid input items
        val (itemEntity, recipe) = findPickupItem() ?: return
        val stack = itemEntity.stack.copy()

        inventory = ItemStack(stack.item, 1)
        activeRecipe = recipe
        progress = 0

        stack.decrement(1)

        if (!stack.isEmpty) {
            itemEntity.stack = stack
        } else {
            itemEntity.remove()
        }

        markDirty()
        sync()
    }

    private fun findPickupItem(): Pair<ItemEntity, GrindingRecipe>? {
        // Find items laying on top
        val inputArea = VoxelShapes.cuboid(0.0, 1.0, 0.0, 1.0, 1.1, 1.0)
        val items = world!!.getEntitiesByClass(
            ItemEntity::class.java,
            inputArea.boundingBox.offset(pos),
            EntityPredicates.VALID_ENTITY
        )

        if (items.isEmpty()) {
            return null
        }

        // TODO: This probably needs optimization/caching
        for (item in items) {
            // Check if any recipes are valid for this item
            val recipe = getRecipeForInput(item.stack) ?: continue
            return Pair(item, recipe)
        }

        return null
    }

    private fun trySpawnResult() {
        if (world!!.isClient) {
            return
        }

        val entity = ItemEntity(world, pos.x + 0.5, pos.y - 0.5, pos.z + 0.5, activeRecipe!!.output.copy())
        entity.setVelocity(0.0, 0.0, 0.0)
        world!!.spawnEntity(entity)

        world!!.playSound(null, pos.down(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 0.2f, 2.0f)

        inventory = ItemStack.EMPTY
        activeRecipe = null

        markDirty()
        sync()
    }

    override fun toTag(tag: CompoundTag): CompoundTag {
        super.toTag(tag)
        tag.putInt("pr", progress)
        return toClientTag(tag)
    }

    override fun fromTag(state: BlockState?, tag: CompoundTag) {
        super.fromTag(state, tag)
        progress = tag.getInt("pr")
        fromClientTag(tag)
    }

    override fun toClientTag(tag: CompoundTag): CompoundTag {
        super.toClientTag(tag)

        val items = DefaultedList.ofSize(1, inventory)
        Inventories.toTag(tag, items)

        return tag
    }

    override fun fromClientTag(tag: CompoundTag) {
        super.fromClientTag(tag)

        val items = DefaultedList.ofSize(1, ItemStack.EMPTY)
        Inventories.fromTag(tag, items)
        if (items[0].item != Items.AIR && inventory.item != items[0].item) {
            progress = 0
        }
        inventory = items[0]
    }

    override fun getConnections(): List<Direction> {
        val facing = world!!.getBlockState(pos).get(GrinderBlock.FACING)
        val axleFacing = facing.rotateYCounterclockwise()
        return listOf(axleFacing, axleFacing.opposite)
    }

    override fun getLocalInverted(): Boolean {
        val facing = world!!.getBlockState(pos).get(GrinderBlock.FACING)
        val axleFacing = facing.rotateYCounterclockwise()
        return axleFacing.direction == Direction.AxisDirection.POSITIVE
    }

    private fun getRecipeForInput(input: ItemStack): GrindingRecipe? {
        if (recipeCache == null) {
            recipeCache = world!!.recipeManager.listAllOfType(StirlingMod.GRINDING_RECIPE_TYPE)
        }
        return recipeCache!!.firstOrNull { it.input.test(input) }
    }

    companion object {
        private var recipeCache: List<GrindingRecipe>? = null
    }
}