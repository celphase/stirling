package com.celphase.stirling.block

import com.celphase.stirling.block.entity.DebugSpinnerBlockEntity
import net.minecraft.block.Block
import net.minecraft.block.BlockEntityProvider
import net.minecraft.block.entity.BlockEntity
import net.minecraft.world.BlockView

class DebugSpinnerBlock(settings: Settings) : Block(settings), BlockEntityProvider {
    override fun createBlockEntity(world: BlockView): BlockEntity =
        DebugSpinnerBlockEntity()
}
