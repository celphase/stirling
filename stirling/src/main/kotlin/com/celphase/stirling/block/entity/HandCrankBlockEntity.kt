package com.celphase.stirling.block.entity

import com.celphase.stirling.StirlingMod
import com.celphase.stirling.block.HandCrankBlock
import com.celphase.stirling.mechanism.entity.SinglePartBlockEntity
import net.minecraft.util.math.Direction

class HandCrankBlockEntity : SinglePartBlockEntity(StirlingMod.HAND_CRANK_BLOCK_ENTITY) {
    private var ticksRemaining: Int = 0

    override fun tick() {
        super.tick()

        if (!world!!.isClient) {
            // Tick down until we stop moving
            if (ticksRemaining >= 0) {
                ticksRemaining -= 1

                if (ticksRemaining == 0) {
                    applyPower(0)
                }
            }
        }
    }

    override fun getConnections(): List<Direction> {
        val facing = world!!.getBlockState(pos).get(HandCrankBlock.FACING)
        return listOf(facing.opposite)
    }

    override fun getLocalInverted(): Boolean {
        val facing = world!!.getBlockState(pos).get(HandCrankBlock.FACING)
        return facing.direction == Direction.AxisDirection.POSITIVE
    }

    fun onCrank(multiplier: Int) {
        applyPower(10 * multiplier)
        ticksRemaining = 10
    }

    private fun applyPower(velocity: Int) {
        assert(!world!!.isClient)
        part?.mechanism?.applyVelocity(velocity * localMultiplier)
    }
}