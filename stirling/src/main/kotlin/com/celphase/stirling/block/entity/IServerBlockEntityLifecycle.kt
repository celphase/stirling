package com.celphase.stirling.block.entity

interface IServerBlockEntityLifecycle {
    fun onServerLoad() {}
    fun onServerUnload() {}
}