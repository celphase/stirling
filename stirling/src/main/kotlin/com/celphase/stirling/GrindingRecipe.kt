package com.celphase.stirling

import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.recipe.Ingredient
import net.minecraft.recipe.Recipe
import net.minecraft.recipe.RecipeSerializer
import net.minecraft.recipe.RecipeType
import net.minecraft.util.Identifier
import net.minecraft.world.World

class GrindingRecipe(
    private val id: Identifier,
    val input: Ingredient,
    private val output: ItemStack,
) : Recipe<Inventory> {
    override fun matches(inv: Inventory, world: World): Boolean = input.test(inv.getStack(0))

    override fun craft(inv: Inventory): ItemStack = output.copy()

    override fun fits(width: Int, height: Int): Boolean = true

    override fun getOutput(): ItemStack = output

    override fun getId(): Identifier = id

    override fun getSerializer(): RecipeSerializer<*> = StirlingMod.GRINDING_RECIPE_SERIALIZER

    override fun getType(): RecipeType<*> = StirlingMod.GRINDING_RECIPE_TYPE
}