plugins {
    id("fabric-loom")
    kotlin("jvm") version "1.4.21"
}

group = "com.celphase"
version = "0.1.0"

val minecraftVersion: String by project
val yarnVersion: String by project
val fabricLoaderVersion: String by project
val fabricApiVersion: String by project
val fabricKotlinVersion: String by project

repositories {
    maven {
        name = "Ladysnake Libs"
        url = uri("https://dl.bintray.com/ladysnake/libs")
    }
}

dependencies {
    minecraft(minecraftVersion)
    mappings(yarnVersion)
    modImplementation(fabricLoaderVersion)
    modImplementation(fabricApiVersion)
    modImplementation(fabricKotlinVersion)

    modImplementation("io.github.onyxstudios:Cardinal-Components-API:2.7.9")
}

tasks {
    processResources {
        inputs.property("version", project.version)

        filesMatching("fabric.mod.json") {
            expand("version" to project.version)
        }
    }

    register<Jar>("sourcesJar") {
        archiveClassifier.set("sources")
        from(sourceSets["main"].allSource)
    }

    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }

    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}
