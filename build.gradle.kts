plugins {
    id("fabric-loom") version "0.5-SNAPSHOT"
}

// This is a dummy build that just puts together all the subprojects

val minecraftVersion: String by project
val yarnVersion: String by project
val fabricLoaderVersion: String by project
val fabricApiVersion: String by project

dependencies {
    minecraft(minecraftVersion)
    mappings(yarnVersion)
    modImplementation(fabricLoaderVersion)
    modImplementation(fabricApiVersion)

    implementation(project(":stirling"))
}
